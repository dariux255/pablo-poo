﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pablo_9
{
    class Pelicula
    {
        private string Titulo;
        private int Duracion;
        private int Edad_Min;
        private string Director;

        public Pelicula(string tit, int dur, int ed, string dir)
        {
            Titulo = tit;
            Duracion = dur;
            Edad_Min = ed;
            Director = dir;
        }

        public string get_tit()
        {
            return (Titulo);
        }
        public int get_dur()
        {
            return (Duracion);
        }
        public int get_edad()
        {
            return (Edad_Min);
        }
        public string get_dir()
        {
            return (Director);
        }
        
    }

    class Espectador
    {
        private string Nombre;
        private int Edad;
        private int Dinero;
        private string asiento;
        private string asientoInt;
        private bool sentado;
        private string[] ABC = new string[] { "a", "b", "c", "d", "e", "f", "g", "h", "i" };

        public Espectador(string nom, int edad, int din)
        {
            Nombre = nom;
            Edad = edad;
            Dinero = din;
        }

        public void Sentarse(int numero, int letra)
        {
            asientoInt = letra.ToString() + (numero + 1).ToString();            //es para mas adelante fijarse si ese asiento está ocupado
            asiento = ABC[letra] + (numero + 1).ToString();
            sentado = true;
        }

        public string get_nom()
        {
            return (Nombre);
        }
        public int get_edad()
        {
            return (Edad);
        }
        public int get_din()
        {
            return (Dinero);
        }
        public string get_asi()
        {
            return (asiento);
        }
        public void setParado()
        {
            sentado = false;
        }
        public string get_asiento()
        {
            return asientoInt;
        }
    }

    class Asiento
    {
        public Asiento(string letra, int numero)
        {
            Letra = letra;
            Num = numero;
        }
        public Asiento(string letra, int numero, bool ocupado)
        {
            Letra = letra;
            Num = numero;
            Ocupado = true;
        }

        public (string , string) getxd()        //imprime el codigo de asiento
        {
            return (Letra, (Num+1).ToString());
        }

        public string Letra;
        public int Num;
        public bool Ocupado=false;

        public int getOcupado()
        {
            if (Ocupado == true)
                return 1;
            else
                return 0;
        }
    }

    class Cine
    {
        private Pelicula Pelicula;
        private int Precio = 50;
        private int letra;
        private int numer;
        private Asiento[,] Asientos = new Asiento[9, 9];
        private string[] ABC = new string[] { "a", "b", "c", "d", "e", "f", "g", "h", "i" };

        public Cine(Pelicula Peli)
        {
            Pelicula = Peli;
            for (int y = 0; y <= 8; y++)
            {
                for (int x = 0; x <= 7; x++)
                {
                    Asiento asiento = new Asiento(ABC[y], x);
                    Asientos[y, x] = asiento;

                }
            }
        }


        public Asiento[,] getdisponibles
        {
            get { return Asientos; }
        }
        public void setOcupado(int numero, int letra){
            Asientos[letra, numero].Ocupado = true;
        }

        public int getPrecio()
        {
            return Precio;
        }

    }


    class Program
    {
        static void Main(string[] args)
        {
            List<Espectador> espectadores = new List<Espectador>();
            Random random = new Random();
            Pelicula peli = new Pelicula("pene", 12, 12, "penecito");
            Cine Cine = new Cine(peli);

            for (int i = 0; i < 5; i++)
            {
                int random_1 = random.Next(0, 8);
                int random_2 = random.Next(0, 9);
                Espectador esp = new Espectador("juan"+ i.ToString(), 10+i, 407+i);
                if (esp.get_din() >= Cine.getPrecio() && esp.get_edad()>=peli.get_edad())
                {
                    esp.Sentarse(random_1, random_2);
                    Cine.setOcupado(random_1, random_2);
                }
                else
                    esp.setParado();
                espectadores.Add(esp);

                foreach (var A in espectadores) {
                    if (random_1.ToString() + random_2.ToString() == esp.get_asiento())
                    {
                        int random_alt = random.Next(0, 8);
                        int random_alt2 = random.Next(0, 9);
                        esp.Sentarse(random_alt, random_alt2);

                    }
                }
            }






            Console.WriteLine("Espectadores: "+ Environment.NewLine);
            
            foreach(var esp in espectadores)
            {
                Console.WriteLine(esp.get_nom() + " " + esp.get_edad() + " " + esp.get_din() + " " + esp.get_asi());
            }

            Console.WriteLine(Environment.NewLine + "Asientos : (asientos + ocupado (1) o libre (0))" + Environment.NewLine);

            for (int y = 0; y <= 8; y++)
            {
                for (int x = 0; x <= 7; x++)
                {
                    Console.Write(Cine.getdisponibles[y, x].getxd() + " "+Cine.getdisponibles[y, x].getOcupado().ToString() + " ");
                }
                Console.WriteLine();
            }
        }
    }
}