﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_pablo
{
    class Electrodomestico
    {
        double Precio_Base = 100;
        string Color = "Blanco";
        char Consumo_Electrico = 'F';
        int Peso = 5;

        public Electrodomestico()
        {

        }
        public Electrodomestico(int precio, int peso)
        {
            Precio_Base = precio;
            Peso = peso;
            comprobarColor();
            comprobarConsumoEnergetico();
        }
        public Electrodomestico(int precio, int peso, string color, char consumo)
        {
            Precio_Base = precio;
            Peso = peso;
            Color = color;
            Consumo_Electrico = consumo;
            comprobarColor();
            comprobarConsumoEnergetico();
        }



        public double Get_precio()
        {
            return (Precio_Base);
        }

        public int Get_peso()
        {
            return (Peso);
        }

        public string Get_color()
        {
            return (Color);
        }

        public char Get_consumo()
        {
            return (Consumo_Electrico);
        }

        public void set_Precio(double x)
        {
            Precio_Base = x;
        }

        private void comprobarConsumoEnergetico()
        {
            if (Consumo_Electrico != 'A' || Consumo_Electrico != 'B' || Consumo_Electrico != 'C' || Consumo_Electrico != 'D' || Consumo_Electrico != 'E' || Consumo_Electrico != 'F')
                Consumo_Electrico = 'F';
        }

        private void comprobarColor()
        {
            if (Color != "Blanco" || Color != "Negro" || Color != "Rojo" || Color != "Azul" || Color != "Gris")
                Color = "Blanco";
        }

        public double precioFinal()
        {
            if (Consumo_Electrico == 'A')
                Precio_Base += 100;

            if (Consumo_Electrico == 'B')
                Precio_Base += 80;

            if (Consumo_Electrico == 'C')
                Precio_Base += 60;

            if (Consumo_Electrico == 'D')
                Precio_Base += 50;

            if (Consumo_Electrico == 'E')
                Precio_Base += 30;

            if (Consumo_Electrico == 'F')
                Precio_Base += 10;

            if (Peso >= 0 && Peso <= 19)
                Precio_Base += 10;

            if (Peso >= 20 && Peso <= 49)
                Precio_Base += 50;

            if (Peso >= 50 && Peso <= 79)
                Precio_Base += 80;

            if (Peso >= 80)
                Precio_Base += 100;

            return Precio_Base;
        } 
    }

    class Lavadora : Electrodomestico
    {
        int Carga = 5;


        public Lavadora()
        {

        }
        
        public Lavadora(int precio, int peso) : base(precio, peso)
        {
        }

        public Lavadora(int precio, int peso, string color, char consumo, int carga) :base(precio, peso, color, consumo)
        {
            Carga = carga;
        }

        public int getCarga()
        {
            return Carga;
        }

        public double precioFinal()
        {
            double precioF = base.precioFinal();
            if (Carga > 30)
            {
                return (precioF + 50);
            }
            else return precioF;
        }
    }

    class Television : Electrodomestico
    {
        int Resolucion = 20;
        bool Sintonizador = false;

        public Television()
        {

        }

        public Television(int precio, int peso) : base(precio, peso)
        {

        }

        public Television(int precio, int peso, string color, char consumo, int resolucion, bool sintonizador) : base(precio, peso, color, consumo)
        {
            Resolucion = resolucion;
            Sintonizador = sintonizador;
        }


        public int getResolucion()
        {
            return Resolucion;
        }

        public bool getSinto()
        {
            return Sintonizador;
        }


        public double precioFinal()
        {
            double precioF = base.precioFinal();
            if (Resolucion > 40)
            {
                return (precioF*1.33);
            }
            if (Sintonizador == true)
            {
                return (precioF += 50);
            }
            else return precioF;
        }
    }
    


    class Program
    {
        static void Main(string[] args)
        {
            Electrodomestico[] electrodomesticos = new Electrodomestico[10];
            for (int i = 0; i < 5; i++)
            {
                Television tele = new Television(9+i, 50+i, "azul", 'ñ', 38+i, true); 
                electrodomesticos[i] = tele;
            }
            for (int i = 5; i < 10; i++)
            {
                Lavadora lavarropas = new Lavadora(250, 500, "Negro", 'A', 35);
                electrodomesticos[i] = lavarropas;
            }
            Lavadora lavarropas2 = new Lavadora(250, 500, "Negro", 'A', 35);

            Console.WriteLine("base(precio, peso, color, consumo)");

            /* foreach (var x in electrodomesticos)
             {
                 if (x is Lavadora) {
                     Console.WriteLine("Datos de la lavadora");
                     Console.WriteLine("Peso : " + x.Get_peso());
                     Console.WriteLine("Consumo : " + x.Get_consumo());
                     Console.WriteLine("Color : " + x.Get_color());
                     Console.WriteLine("Precio : " + x.precioFinal());
                 }
                 if (x is Television){
                     Console.WriteLine("Datos de la television");
                     Console.WriteLine("Peso : " + x.Get_peso());
                     Console.WriteLine("Consumo : " + x.Get_consumo());
                     Console.WriteLine("Color : " + x.Get_color());
                     Console.WriteLine("Precio : " + x.precioFinal());
                     Console.WriteLine();
                 }*/

            Console.WriteLine("Datos de la lavadora");
            Console.WriteLine("Peso : " + electrodomesticos[5].Get_peso());
            Console.WriteLine("Consumo : " + electrodomesticos[5].Get_consumo());
            Console.WriteLine("Color : " + electrodomesticos[5].Get_color());
            Console.WriteLine("Precio : " + electrodomesticos[5].precioFinal());
            Console.WriteLine("Precio : " + electrodomesticos[5].precioFinal());

            Console.WriteLine("Datos de la lavadora");
            Console.WriteLine("Peso : " + lavarropas2.Get_peso());
            Console.WriteLine("Consumo : " + lavarropas2.Get_consumo());
            Console.WriteLine("Color : " + lavarropas2.Get_color());
            Console.WriteLine("Precio : " + lavarropas2.precioFinal());
        
        }
    }
}
