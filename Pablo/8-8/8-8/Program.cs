﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8_8//nombre, edad y sexo. De los estudiantes, queremos saber también su calificación actual (entre 0 y 10) y del profesor que materia da.
{
    class Alumno
    {
        public Alumno()
        {
            System.Threading.Thread.Sleep(1);
        }
        public Alumno(string N, int E, bool S, int C)
        {
            Nombre = N;
            Edad = E;
            Sexo = S;
            Calif = C;
            Prov();
            System.Threading.Thread.Sleep(1);

        }
        string Nombre;
        int Edad;
        bool Sexo;
        int Calif;
        bool Presente;
        Random Nov = new Random(DateTime.UtcNow.Millisecond);
        public void Prov()
        {
            if (Nov.Next(0, 2) == 1)
                Presente = true;
            else
                Presente = false;
        }
        public string getNombre()
        {
            return Nombre;
        }
        public int getEdad()
        {
            return Edad;
        }
        public bool getSexo()
        {
            return Sexo;
        }
        public int getCalif()
        {
            return Calif;
        }
        public bool getPresente()
        {
            return Presente;
        }
    }
    class Docente
    {
        Random Disp = new Random(DateTime.UtcNow.Millisecond);
        string Nombre;
        int Edad;
        bool Sexo;
        int Materia; //(0, 1 o 2 (matemáticas, filosofía y física))
        bool Presente;

        public void Prov()
        {
            if (Disp.Next(0, 6) == 1)
                Presente = false;
            else
                Presente = true;
        }
        public Docente()
        {
            System.Threading.Thread.Sleep(1);
            Prov();
        }
        public Docente(string N, int E, bool S, int C)
        {
            Nombre = N; Edad = E; Sexo = S; Materia = C;
            Prov();
        }
        public string getNombre()
        {
            return Nombre;
        }
        public int getEdad()
        {
            return Edad;
        }
        public bool getSexo()
        {
            return Sexo;
        }
        public int getMateria()
        {
            return Materia;
        }
        public bool getPresente()
        {
            return Presente;
        }
    }
    class Aula
    {
        int ID = 1;
        int Max = 20;
        int CantAlumnos;
        int cont = 0;
        int Materia; //(0, 1 o 2 (matemáticas, filosofía y física))
        public bool DarClase;
        public Aula(int ID ,List<Alumno> Alumnos, Docente Docente)
        {
            this.ID = ID;

            CantAlumnos = Alumnos.Count();
            foreach (var A in Alumnos)
            {
                if (A.getPresente())
                    cont++;
            }
            if ((CantAlumnos / 2 < cont) && Docente.getPresente() && Docente.getMateria() == ID)
            {
                DarClase = true;
            }
            else
                DarClase = false;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<Alumno> Alumnos = new List<Alumno>();
            Alumno Matias = new Alumno("Matias", 12, true, 10);
            Alumnos.Add(Matias);
            Alumno Jorge = new Alumno("Jorge", 2, false, 6);
            Alumnos.Add(Jorge);
            Alumno Dario = new Alumno("Dario", 5, true, 1);
            Alumnos.Add(Dario);
            Alumno Daria = new Alumno("Daria", 9, false, 7);
            Alumnos.Add(Daria);
            Docente Facundo = new Docente("Facundo", 90, false, 1);
            Aula Aula1 = new Aula(1, Alumnos, Facundo);
            if (Aula1.DarClase)
                foreach (var a in Alumnos)
                {
                    if (a.getSexo() == false && a.getCalif() >= 6)
                        Console.WriteLine(a.getNombre());
                    if (a.getSexo() == true && a.getCalif() >= 6)
                        Console.WriteLine(a.getNombre());
                }
            else
                Console.WriteLine("No se puede dar clase");
        }
    }
}
