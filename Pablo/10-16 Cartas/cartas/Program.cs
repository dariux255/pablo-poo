﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cartas
{
    public class cartas
    {
        int numero;
        string palo;
        List<cartas> Cartas = new List<cartas>();
        List<cartas> Cartas_eliminadas = new List<cartas>();

        public cartas()
        {
            int cont = 0;
            while (cont < 4)
            {
                for (int i = 0; i < 10; i++)
                {
                    int alt = i + 1;
                    string palin = cont.ToString();
                    if (i == 7 || i == 8 || i == 9)
                        alt += 2;
                    if (cont == 0)
                        palin = "espada";
                    if (cont == 1)
                        palin = "basto";
                    if (cont == 2)
                        palin = "oro";
                    if (cont == 3)
                        palin = "copa";
                    cartas carta = new cartas(alt, palin);
                    Cartas.Add(carta);
                }
                cont++;
            }
        }

        public cartas(int Num, string Pal)
        {
            numero = Num;
            palo = Pal;
        }

        public void barajar()
        {
            Random Ran = new Random();
            List<cartas> cartasRan = new List<cartas>();
            int cont = Cartas.Count();
            for (int i = 0; i < cont; i++)
            {
                int Pos = Ran.Next(0, Cartas.Count());
                cartasRan.Add(Cartas[Pos]);
                Cartas.RemoveAt(Pos);
            }
            Cartas = cartasRan;
        }

        public void darCartas()
        {
            Console.WriteLine("Cuantas cartas se retiran?");
            int Cant_cartas = int.Parse(Console.ReadLine());
            if (Cant_cartas >= Cartas.Count())
                Console.WriteLine("No se pueden retirar tantas cartas. Hay {0} cartas actualmente", cartasDisponibles());
            else
                for (int i = 1; i <= Cant_cartas; i++)
                {
                    Cartas_eliminadas.Add(Cartas[1]);
                    Cartas.RemoveAt(1);
                }
        }

        public List<cartas> getCartas()
        {
            return Cartas;
        }

        public int cartasDisponibles()
        {
            return Cartas.Count;
        }

        public void cartasMonton()
        {
            foreach (var C in Cartas_eliminadas)
                Console.WriteLine("las cartas eliminadas son: {0} Numero = {1} Palo = {2}", Environment.NewLine, C.numero, C.palo);
        }

        public void mostrarMazo(cartas Carta)
        {
            foreach (var C in Carta.getCartas())
            {
                Console.WriteLine("numero " + C.numero + " palo " + C.palo);
            }
        }
    }


    class Program
    {
        static void Main(string[] args)
        {

            cartas cartitas = new cartas();
            while (true)
            {
                Console.WriteLine("Presione: {0} 1 Para barajar el mazo {0} 2 Para pedir cartas {0} 3 Para mostrar las cartas faltantes {0} 4 Para mostrar el mazo {0} 5 Para mostrar la cantidad de cartas disponibles {0} 6 Para salir{0}", Environment.NewLine);
                string Control = Console.ReadLine();
                Console.Clear();
                if (Control == "1")
                    cartitas.barajar();
                if (Control == "2")
                {
                    cartitas.darCartas();
                    Console.WriteLine("Operacion realizada");
                    System.Threading.Thread.Sleep(3000);
                }
                if (Control == "3")
                {
                    cartitas.cartasMonton();
                    Console.WriteLine("Presione una tecla para continuar");
                    string Test = Console.ReadLine();
                }
                if (Control == "4")
                {
                    cartitas.mostrarMazo(cartitas);
                    Console.WriteLine("Presione una tecla para continuar");
                    string Test = Console.ReadLine();
                }
                if (Control == "5")
                {
                    Console.WriteLine("Hay {0} Cartas disponibles", cartitas.cartasDisponibles());
                    Console.WriteLine("Presione una tecla para continuar");
                    string Test = Console.ReadLine();
                }
                if (Control == "6")
                {
                    Console.Write("Saliendo");
                    for (int i = 0; i < 3; i++)
                    {
                        System.Threading.Thread.Sleep(500);
                        Console.Write(".");
                    }
                    Console.WriteLine();
                    Environment.Exit(1);
                }
                Console.Clear();
            }
        }
    }
}
