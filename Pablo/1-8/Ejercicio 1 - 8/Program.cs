﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_1___8
{
    class cuenta
    {
        string titular;
        double cantidad;
        public cuenta(string tit)
        {
            titular = tit;
        }
        public cuenta(string tit, double cant)
        {
            titular = tit;
            cantidad = cant;
        }
        public void setTitular(string titular)
        {
            this.titular = titular;
        }
        public string getTitular()
        {
            return titular;
        }

        public void setCantidad(int cant)
        {
            cantidad = cant;
        }
        public double getCantidad()
        {
            return cantidad;
        }


        public bool ingresar(double cantidad)
        {
            if (cantidad < 0)
            {
                return false;
            }
            else
            {
                this.cantidad += cantidad;
                return true;
            }
        }
        public double retiro(double monto)
        {
            if (this.cantidad - monto < 0)
                this.cantidad = 0;
            else
                this.cantidad -= monto;

            return cantidad;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            cuenta pepe12 = new cuenta("pepe", 23);
            Console.WriteLine(pepe12.getTitular() + pepe12.getCantidad());
       //     Console.WriteLine(pepe12.getCantidad());
        }
    }
}
