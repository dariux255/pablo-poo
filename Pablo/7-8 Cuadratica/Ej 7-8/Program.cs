﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ej_7_8
{
    public class Raices
    {
        double A;
        double B;
        double C;
        public Raices()
        {
        }
        public Raices(double a)
        {
            A = a;
        }
        public Raices(double a, double b)
        {
            A = a;
            B = b;
        }
        public Raices(double a, double b, double c)
        {
            A = a;
            B = b;
            C = c;
        }
        public (double, double) ObtenerRaices()
        {
            double a = this.A;
            double b = this.B;
            double c = this.C;
            double z = 0;

            double Form1 = (-b + Math.Sqrt(((b * b) - 4 * a * c))) / (2 * a);
            double Form2 = (-b - Math.Sqrt(((b * b) - 4 * a * c))) / (2 * a);
            if (Form1 == Form2 || (double.IsNaN(Form1) || double.IsNaN(Form2)))
                return (z / z, z / z);
            else 
                return (Form2, Form1);
           
        }

        public double ObtenerRaiz()
        {
            double a = this.A;
            double b = this.B;
            double c = this.C;
            double z = 0;

            double Form1 = (-b + Math.Sqrt(((b * b) - 4 * a * c))) / (2 * a);
            double Form2 = (-b - Math.Sqrt(((b * b) - 4 * a * c))) / (2 * a);
            if (Form1 != Form2)
            {
                return (z / z);
            }
            else
                return (Form1);
        }
        public double ObtenerDiscriminante()
        {
            double a = this.A;
            double b = this.B;
            double c = this.C;

            double Form1 = (b * b) - 4 * a * c;
            return Form1;
        }

        public bool TieneRaices()
        {
            if (ObtenerDiscriminante() > 0)
            {
                return true;
            }
            else { return false; }
        }

        public bool TieneRaiz()
        {
            if (ObtenerDiscriminante() == 0)
            {
                return true;
            }
            else { return false; }
        }


        public string[] Ejecut()
        {
            string[] Resultados = new string[5];
            
            var (R2, R1) = ObtenerRaices();
            if (double.IsNaN(R1) || double.IsNaN(R2))
                Resultados[0] = "Esta función tiene menos de dos raices";
            else { 
                if (R1 < R2)
                    Resultados[0] = "Las raices son : " + R1 + " y " + R2;
                else
                    Resultados[0] = "Las raices son : " + R2 + " y " + R1;
                }

            if (double.IsNaN(ObtenerRaiz()))
                Resultados[1] = "Esta función tiene una o ninguna raiz";
            else
                Resultados[1] = "La raiz de esta funcion es: " + ObtenerRaiz();

            Resultados[2] = "El discriminante de la función es: " + ObtenerDiscriminante();

            if (TieneRaices())
                Resultados[3] = "Esta función tiene dos Raices";
            else
                Resultados [3] = "Esta función tiene menos de 2 Raices";


            if (TieneRaiz())
                Resultados[4] = "Esta funcion tiene solo una raiz";
            else
                Resultados[4] = "esta funcion no tiene solo una raiz";

            return Resultados;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Raices Fx;
            Console.WriteLine("Ingresar coeficiente de mayor orden");
            string A = Console.ReadLine();
            Console.Clear();
            if (string.IsNullOrEmpty(A) || A == "0")
            {
                Console.WriteLine("Debe ingresar por lo menos un valor en este campo");
                System.Threading.Thread.Sleep(1000);
                Console.WriteLine("Ingresar coeficiente de mayor orden");
                A = Console.ReadLine();
            }
            Console.WriteLine("Ingresar coeficiente de menor orden");
            string B = Console.ReadLine();
            Console.Clear();
            if (string.IsNullOrEmpty(B))
            {
                Console.WriteLine("Si la funcion no tiene este termino, ingrese 0");
                Console.WriteLine("Ingresar coeficiente de menor orden");
                B = Console.ReadLine();
                Console.Clear();
            }
            Console.WriteLine("Ingresar termino independiente");
            string C = Console.ReadLine();
            Console.Clear();
            
            if (string.IsNullOrEmpty(C))
            {
                Console.WriteLine("Si la funcion no tiene este termino, ingrese 0");
                Console.WriteLine("Ingresar termino independiente");
                C = Console.ReadLine();
                Console.Clear();
            }

            Fx = new Raices(double.Parse(A), double.Parse(B), double.Parse(C));
            Console.WriteLine("Su funcion es: " + A + "²"+ "+"+ B +"x" + "+" + C);
            string[] Arr = Fx.Ejecut();

            for (int I = 0; I < Arr.Length; I++)
            {
                Console.WriteLine(Arr[I]);
                Console.WriteLine();
            }
        }
    }
}
