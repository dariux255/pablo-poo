﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ej_7_8;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ej_7_8.Tests
{
    [TestClass()]
    public class RaicesTests
    {
        Raices xd = new Raices(2, 2);
        Raices xd2 = new Raices(2, 2, 1);
        [TestMethod()]
        public void ObtenerRaicesTest()
        {
            Assert.AreEqual((-1, 0), xd.ObtenerRaices());
        }

        [TestMethod()]
        public void ObtenerRaizTest()
        {

            Assert.IsTrue(Double.IsNaN(xd.ObtenerRaiz()));
        }

        [TestMethod()]
        public void ObtenerDiscriminanteTest()
        {
            Assert.AreEqual(4, xd.ObtenerDiscriminante());
        }
        [TestMethod()]
        public void ObtenerDiscriminanteTest2()
        {
            Assert.AreEqual(-4, xd2.ObtenerDiscriminante());
        }

        [TestMethod()]
        public void TieneRaicesTest()
        {
            Assert.AreEqual(true, xd.TieneRaices());
        }

        [TestMethod()]
        public void TieneRaizTest()
        {
            //Assert.AreEqual(true, xd.ObtenerRaiz());
            Assert.IsTrue(Double.IsNaN(xd.ObtenerRaiz()));
        }
    }
}