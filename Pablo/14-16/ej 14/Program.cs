﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ej_14
{
    class Productos
    {
        string Nombre;
        double Precio;
        public Productos(string N, int P)
        {
            Nombre = N;
            Precio = P;
        }

        public double precio(int cant)
        {
            return (Precio *= cant);
        }

        public void set_nombre(string x)
        {
            Nombre = x;
        }
        public void set_precio(double x)
        {
            Precio = x;
        }
        public string get_nombre()
        {
            return Nombre;
        }
        public double get_precio()
        {
            return Precio;
        }
    }
    class Perecedero : Productos
    {
        int Dias_a_caducir;
        public Perecedero(string N, int P, int D) : base(N, P)
        {
            Dias_a_caducir = D;
        }
        public int get_caducidad()
        {
            return Dias_a_caducir;
        }
        public void set_caducidad(int x)
        {
            Dias_a_caducir = x;
        }
        public void precio(double cant)
        {
            if (Dias_a_caducir == 1)
                set_precio(get_precio() * 1 / 4 * cant);
            if (Dias_a_caducir == 2)
                set_precio(get_precio() * 1 / 3 * cant);
            if (Dias_a_caducir == 3)
                set_precio(get_precio() * 1 / 2 * cant);
        }
    }
    class No_perecedero : Productos
    {
        string Tipo;
        public No_perecedero(string N, int P, string T) : base(N, P)
        {
            Tipo = T;
        }
        public string get_tipo()
        {
            return Tipo;
        }
        public void set_caducidad(string x)
        {
            Tipo = x;
        }
        public void precio(double cant)
        { 
            set_precio(base.get_precio() * cant);
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            List<Productos> Productos = new List<Productos>();
            No_perecedero galletitas = new No_perecedero("galletitas", 50, "comestibles");
            Perecedero manzana = new Perecedero("manzana", 10, 1);
            No_perecedero carne = new No_perecedero("carne", 200,"carne");
            No_perecedero arvejas = new No_perecedero("arvejas", 30, "comestibles");
            Perecedero banana = new Perecedero("banana", 5, 3);


            Productos.Add(galletitas);
            Productos.Add(manzana);
            Productos.Add(carne);
            Productos.Add(arvejas);
            Productos.Add(banana);
            foreach (var P in Productos)
            {
                if (P is No_perecedero)
                {
                    No_perecedero aux = (No_perecedero)P;
                    Console.WriteLine(aux.get_nombre() + " este producto no perecedero es de tipo: " + aux.get_tipo() + " y sale: " + aux.get_precio());
                }
                if (P is Perecedero)
                {
                    Perecedero aux = (Perecedero)P;
                    Console.WriteLine(aux.get_nombre() + " este producto perecedero es: " + aux.get_nombre() + " vence en " + aux.get_caducidad() + " dias" + " y sale: " + aux.get_precio());
                }
            }

            Console.WriteLine(Environment.NewLine + "///////////////////precios cambiados" + Environment.NewLine);

            foreach (var P in Productos)
            {
                if (P is No_perecedero)
                {
                    No_perecedero aux = (No_perecedero)P;
                    aux.precio(1);
                    Console.WriteLine(aux.get_nombre() + " este producto no perecedero es de tipo: " + aux.get_tipo() + " y sale: "+aux.get_precio());
                }
                if (P is Perecedero)
                {
                    Perecedero aux = (Perecedero)P;
                    aux.precio(1);
                    Console.WriteLine(aux.get_nombre() + " este producto perecedero es: " + aux.get_nombre() + " vence en " + aux.get_caducidad() + " dias" + " y sale: " + aux.get_precio());
                }
            }

        }
    }
}
