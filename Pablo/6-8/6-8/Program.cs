﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6_8
{
    class Libro
    {
        int ISBN;
        string Titulo;
        string Autor;
        int N_Pag;

        public void SetISBN(int x)
        {
            ISBN = x;
        }
        public void SetTitulo(string x)
        {
            Titulo = x;
        }
        public void SetAutor(string x)
        {
            Autor = x;
        }
        public void SetPag(int x)
        {
            N_Pag = x;
        }
        public int GetISBN()
        {
            return ISBN;
        }
        public int GetNP()
        {
            return N_Pag;
        }
        public string GetTitulo()
        {
            return Titulo;
        }
        public string GetAutor()
        {
            return Autor;
        }

        public void MostrarInfo()
        {
            Console.WriteLine("El libro '" + Titulo + "' con ISBN = " + ISBN + " creado por el autor " + Autor+" tiene "+N_Pag+" páginas ");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            List<Libro> libros = new List<Libro>();
            Libro Libro1 = new Libro();
            libros.Add(Libro1);
            Libro Libro2 = new Libro();
            libros.Add(Libro2);
            Libro1.SetAutor("Juanita");
            Libro1.SetISBN(123654);
            Libro1.SetPag(90);
            Libro1.SetTitulo("Las noventa paginas de juanita");
            Libro2.SetAutor("Pepe");
            Libro2.SetISBN(987654);
            Libro2.SetPag(153);
            Libro2.SetTitulo("Un libro interesante");
            Console.WriteLine("Libro 1: ");
            Console.SetCursorPosition(4, 1);
            Libro1.MostrarInfo();
            Console.WriteLine("Libro 2: ");
            Console.SetCursorPosition(4, 3);
            Libro2.MostrarInfo();

            Libro comp = new Libro();
            comp.SetPag(0);
            foreach(var L in libros)
            {
                if (L.GetNP() > comp.GetNP())
                {
                    comp.SetPag(L.GetNP());
                    comp.SetTitulo(L.GetTitulo());
                }
            }
            Console.WriteLine(Environment.NewLine + "El libro: '" + comp.GetTitulo() + "' Tiene mas paginas, teniendo un total de " + comp.GetNP());
        }
    }
}
