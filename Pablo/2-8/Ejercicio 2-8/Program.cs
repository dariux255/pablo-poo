﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_2_8
{
    class personas
    {
        string nombre = " "; int edad = 0; int dni; string sexo = "h"; int peso = 0; double altura = 0;
        
        public personas()
        {
            dni = generarDNI();
        }

        public personas(string nombre, int edad, string sexo)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.sexo = sexo;
            this.dni = generarDNI();
        }
        public personas(string nombre, int edad, string sexo, int peso, double altura)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.dni = generarDNI();
            this.sexo = sexo;
            this.peso = peso;
            this.altura = altura;
        }
        public int calcularIMC()
        {

            if (this.peso / (this.altura * this.altura) < 20)
                return -1;

            if ((this.peso / (this.altura * this.altura) > 25))
                return 1;

            else
                return 0; 

            /*calcularIMC(): calculara si la persona esta en su peso ideal (peso en kg/(altura^2  en m)), 
             * si esta fórmula devuelve un valor menor que 20, la función devuelve un -1, si devuelve un número
             * entre 20 y 25 (incluidos), significa que esta por debajo de su peso ideal la función devuelve un 0 
             * y si devuelve un valor mayor que 25 significa que tiene sobrepeso, la función devuelve un 1*/
        }

        public bool esMayorDeEdad()
        {
            if (edad >= 18)
                return true;
            else
                return false;
        }
        public void comprobarSexo(string sexo)
        {
            if (sexo != "h" && sexo != "m")
                this.sexo = "h";
        }
        public int generarDNI()
        {
            List<int> dni = new List<int>();
            Random numrandom = new Random();
            int num = numrandom.Next(10000000, 100000000);
            return num;
        }

        public void setPeso(int x)
        {
            peso = x;
        }
        public void setAltura(double x)
        {
            altura = x;
        }
        public void setEdad(int x)
        {
            edad = x;
        }
        public void setSexo(string x)
        {
            sexo = x;
        }
        public void setNombre(string x)
        {
            nombre = x;
        }
        public void setDNI()
        {
            dni=generarDNI();
        }


        public int getEdad()
        {
            return edad;
        }
        public string getNombre()
        {
            return nombre;
        }
        public int getDni()
        {
            return dni;
        }
        public string getSexo()
        {
            return sexo;
        }
        public int getPeso()
        {
            return peso;
        }
        public double getAltura()
        {
            return altura;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            personas[] gente = new personas[3];

            //Pide por teclado el nombre, la edad, sexo, peso y altura.
            Console.WriteLine("--------------------Persona 1--------------------");
            Console.WriteLine("ingrese el nombre");
            string nombre = Console.ReadLine();
            Console.WriteLine("ingrese la edad");
            int edad = int.Parse(Console.ReadLine());
            Console.WriteLine("ingrese el sexo (h/m)");
            string sexo = Console.ReadLine();
            Console.WriteLine("ingrese el peso");
            int peso = int.Parse(Console.ReadLine());
            Console.WriteLine("ingrese la altura en metros (por ejemplo: 1,80)");
            double altura = double.Parse(Console.ReadLine());
            Console.Clear();

            Console.WriteLine("--------------------Persona 2--------------------");
            Console.WriteLine("ingrese el nombre");
            string p2_nombre = Console.ReadLine();
            Console.WriteLine("ingrese la edad");
            int p2_edad = int.Parse(Console.ReadLine());
            Console.WriteLine("ingrese el sexo (h/m)");
            string p2_sexo = Console.ReadLine();
            Console.Clear();

            //se crean y agregan las personas
            gente [0] = new personas(nombre, edad, sexo, peso, altura);
            gente [1] = new personas(p2_nombre, p2_edad, p2_sexo);
            gente[2] = new personas();
            gente[2].setAltura(1.20); gente[2].setEdad(83); gente[2].setNombre("Pedro"); gente[2].setPeso(80); gente[2].setSexo("m"); gente[2].setDNI();


            int cont = 1;
            for(int i = 0; i<3; i++) 
            {
                Console.WriteLine(Environment.NewLine + "---------Persona " + cont + " -" + " Los datos son:---------");
                Console.WriteLine("nombre: "+gente[i].getNombre());
                Console.WriteLine("edad: "+gente[i].getEdad());
                Console.WriteLine("sexo: "+gente[i].getSexo());
                Console.WriteLine("peso: "+gente[i].getPeso());
                Console.WriteLine("altura: "+gente[i].getAltura());
                Console.WriteLine("dni: "+ gente[i].getDni());
                if (gente[i].calcularIMC() == -1)
                    Console.WriteLine("El sujeto esta por debajo de su peso ideal");
                if (gente[i].calcularIMC() == 0)
                    Console.WriteLine("El sujeto esta en su peso ideal");
                if (gente[i].calcularIMC() == 1)
                    Console.WriteLine("El sujeto esta sobre su peso ideal");
                if (gente[i].esMayorDeEdad())
                    Console.WriteLine("Esta persona es mayor de edad");
                else
                    Console.WriteLine("Esta persona no es mayor de edad");
                cont++;
            }
        }
    }
}
