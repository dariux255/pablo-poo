﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ej_16
{
    class Contacto
    {
        string Nombre;
        int Telefono;

        public Contacto(string N, int T)
        {
            Nombre = N;
            Telefono = T;
        }
        public string get_nombre()
        {
            return Nombre;
        }
        public int get_telefono()
        {
            return Telefono;
        }
    }

    class Agenda
    {
        List<Contacto> Contactos = new List<Contacto>();

        public Agenda()
        {
            Contactos.Capacity = 10;
            Console.WriteLine("cuantos contactos va a agendar?");
            int cant_agendar = int.Parse(Console.ReadLine());
            for (int i = 0; i < cant_agendar; i++)
            {
                Console.WriteLine("ingrese un nombre y luego un numero de telefono");
                Contacto contacto = new Contacto(Console.ReadLine(), int.Parse(Console.ReadLine()));
                Contactos.Add(contacto);
            }
        }

        public Agenda(int Longitud)
        {
            Contactos.Capacity = Longitud;
            Console.WriteLine("cuantos contactos va a agendar?");
            int cant_agendar = int.Parse(Console.ReadLine());
            for (int i = 0; i < cant_agendar; i++)
            {
                Console.WriteLine("ingrese un nombre y luego un numero de telefono");
                Contacto contacto = new Contacto(Console.ReadLine(), int.Parse(Console.ReadLine()));
                Contactos.Add(contacto);
            }
        }


        public void Añadir_Contacto()
        {
            if (!Agenda_llena())
            {
                Console.WriteLine("cuantos contactos va a agendar?");
                int cant_agendar = int.Parse(Console.ReadLine());
                if (Espacios_libres()-cant_agendar>= 0)
                {
                    for (int i = 0; i < cant_agendar; i++)
                    {
                        string nombre = Console.ReadLine();
                        while (Buscar_Contacto(nombre))
                        {
                            Console.WriteLine("ese contacto ya existe");
                            nombre = Console.ReadLine();
                        }
                        int numero = int.Parse(Console.ReadLine());
                        Console.WriteLine("ingrese un nombre y luego un numero de telefono");
                        Contacto ContactoAñadir = new Contacto(nombre, numero);
                        Contactos.Add(ContactoAñadir);
                    }
                }
                else
                    Console.WriteLine("la agenda no permite tantos contactos");
            }
        }


        public void Listar_Contactos()
        {
            foreach(var a in Contactos)
                Console.WriteLine(a.get_nombre() + " telefono:" + a.get_telefono());
        }
        public void Buscar_Contacto()
        {
            Console.WriteLine("ingresar contacto a buscar");
            string busqueda = Console.ReadLine();
            foreach (var a in Contactos)
                if (a.get_nombre() == busqueda)
                    Console.WriteLine(a.get_nombre() + " telefono:" + a.get_telefono());
        }
        public bool Buscar_Contacto(string nombre)
        {
            foreach (var a in Contactos)
                if (a.get_nombre() == nombre)
                {
                    return true;
                } 
                    return false;
        }


        public void Eliminar_Contacto()
        {
            Console.WriteLine("ingresar contacto a eliminar");
            string busqueda = Console.ReadLine();

            for(int i = Contactos.Count()-1; i>=0; i--)
                if (Contactos[i].get_nombre() == busqueda)
                {
                    Contactos.RemoveAt(i);
                }
        }
        public bool Agenda_llena()
        {
            if (Contactos.Count() == Contactos.Capacity)
                return true;
            else
                return false;
        }
        public int Espacios_libres()
        {
            return Contactos.Capacity - Contactos.Count();
        }


    }

    class Program
    {
        static void Main(string[] args)
        {
            Agenda Agendita;
            Console.WriteLine("ingrese la cantidad de personas a agendar");
             string cantidad = Console.ReadLine();
            if(cantidad == null)
            Agendita = new Agenda();
            else{
             Agendita = new Agenda(int.Parse(cantidad));
          }
            while (true)
            {
                Console.WriteLine("Ingrese:" + Environment.NewLine + "'1' para Añadir un contacto");
                Console.WriteLine("'2' para Listar contactos");
                Console.WriteLine("'3' para Buscar un contacto");
                Console.WriteLine("'4' para Eliminar un contacto");
                Console.WriteLine("'5' para verificar si la Agenda está llena");
                Console.WriteLine("'6' para ver los Espacios libres");

                int dato = int.Parse(Console.ReadLine());
                if (dato == 1)
                {
                    Console.Clear();
                    Agendita.Añadir_Contacto();
                }
                if (dato == 2)
                {
                    Console.Clear();
                    Agendita.Listar_Contactos();
                }
                if (dato == 3)
                {
                    Console.Clear();
                    Agendita.Buscar_Contacto();
                }
                if (dato == 4)
                {
                    Console.Clear();
                    Agendita.Eliminar_Contacto();
                }
                if (dato == 5)
                {
                    Console.Clear();
                    if (Agendita.Agenda_llena())
                        Console.WriteLine("La agenda está llena");
                    else
                        Console.WriteLine("La agenda no está llena");
                }
                if (dato == 6)
                {
                    Console.Clear();
                    Console.WriteLine(Agendita.Espacios_libres());
                }

            }

        }
    }
}
