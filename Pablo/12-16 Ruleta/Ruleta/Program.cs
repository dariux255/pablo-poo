﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ruleta
{
    class Revolver
    {
        int Pos_tambor;
        int Pos_bala;
        public Revolver()
        {
            Random random = new Random();
            Pos_bala = random.Next(0, 8);
            Pos_tambor = random.Next(0, 8);
        }

        public int getBala()
        {
            return Pos_bala;
        }
        public int getTambor()
        {
            return Pos_tambor;
        }

        public bool Disparar()
        {
            if (Pos_tambor == Pos_bala)
                return (true);
            else
                return (false);
        }

        public void Pasar_bala()
        {
            Pos_tambor += 1;
            if (Pos_tambor >= 8)
                Pos_tambor = 0;
        }
    }

    class Jugador
    {
        public int ID;
        public string Nombre;
        public bool Vivo;

        public Jugador(int id)
        {
            ID = id;
            Nombre = "Jugador" + id.ToString();
            Vivo = true;
        }

        public void Disparar(bool Disparo) // el bool se obtiene pasando el return del "disparar" de revolver
        {
            if (Disparo)
                Vivo = false;
        }
        public bool getVivo()
        {
            return Vivo;
        }
        public void setVivo(bool x)
        {
            Vivo = x;
        }
    }

    class Juego
    {
        public Juego(List<Jugador> players)
        {
            Jugadores = players;
        }
        List<Jugador> Jugadores;
        Revolver Datos_Revolver;

        public void Fin_juego()
        {
            foreach(var J in Jugadores)
                if (J.getVivo() == false)
                {
                    Console.WriteLine("Programa terminado");
                    Environment.Exit(1);
                }
        }

        public void Ronda(List<Jugador> jugadores, Revolver revolver)// el bool se obtiene pasando el return del "disparar" de revolver
        {
            foreach (var J in jugadores)
            {
                if (revolver.Disparar())
                    J.setVivo(false);
                Console.Write("ID = " + J.ID + " nombre = " + J.Nombre + " vivo = " + J.Vivo + " ");
                Console.WriteLine(" pos bala | pos tambor = " + revolver.getBala()+ " " + revolver.getTambor());
                revolver.Pasar_bala();
            }
            System.Threading.Thread.Sleep(3000*jugadores.Count());
           // Console.Clear();
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Revolver Revolver = new Revolver();
                Jugador Jugador;
                List<Jugador> jugadores = new List<Jugador>();

                Console.WriteLine("Ingrese cantidad de jugadores");
                int Cantidad_jugadores = int.Parse(Console.ReadLine());
                if (Cantidad_jugadores > 6)
                    Cantidad_jugadores = 6;

                for (int i = 0; i < Cantidad_jugadores; i++)
                {
                    Jugador = new Jugador(i + 1);
                    jugadores.Add(Jugador);
                }

                Juego Juego = new Juego(jugadores);
                Juego.Ronda(jugadores, Revolver);
                Juego.Fin_juego();
            }
        }
    }
}
