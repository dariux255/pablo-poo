﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11
{
    class Jugador
    {
        int id;
        int RondasGanadas = 0;
        int Dinero = 1;
        static Random NumRan = new Random();
        int Prediccion = NumRan.Next(0, 6);

        public int getID()
        {
            return id;
        }
        public void setID(int x)
        {
            id = x;
        }
        public int getPrediccion()
        {
            return Prediccion;
        }
        public void setPrediccion(int x)
        {
            Prediccion = x;
        }
        public int getDinero()
        {
            return Dinero;
        }
        public void setDinero(int x)
        {
            Dinero = x;
        }
        public int getRondasGanadas()
        {
            return RondasGanadas;
        }
        public void setRondasGanadas(int x)
        {
            RondasGanadas = x;
        }
    }
    
    class Juego
    {
        int Ronda;
        int pozo;
        static Random NumRan = new Random();
        int resultado = NumRan.Next(0, 6);
        List<Jugador> Jugadores = new List<Jugador>();

        public bool TieneDinero(Jugador Jugador)
        {
            if (Jugador.getDinero() > 0)
                    return true;    
            return false;
        }

        public void Jugar()
        {
            foreach (var i in Jugadores)
                if (TieneDinero(i))
                    i.setDinero(i.getDinero() - 1);
                
        }

        public void Resultados()
        {
            foreach (var i in Jugadores)
                if (i.getPrediccion() == resultado && TieneDinero(i))
                {
                    i.setRondasGanadas(i.getRondasGanadas() + 1);
                    Console.WriteLine("El ganador es el jugador " + i.getID());
                }
            Console.WriteLine("nadie gana");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
        }
    }
}
