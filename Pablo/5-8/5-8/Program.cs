﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5_8
{
    interface Entregable{
        void entregar();
        void devolver();
        bool isEntregado();
        Entregable CompareTo(Entregable xd); //compara series con videojuegos y viceversa 
    }

    class Serie : Entregable
    {
        string Titulo = "";
        int N_Temp = 3;
        bool Entregado = false;
        string Gen = "";
        string Creador = "";

        public Serie()
        {

        }
        public Serie(string T, String C)
        {
            Titulo = T;
            Creador = C;
        }
        public Serie(string T, int NT, string G, string C)
        {
            Titulo = T;
            N_Temp = NT;
            Gen = G;
            Creador = C;
        }
        public void SetTit(string x)
        {
            Titulo = x;
        }
        public void SetNT(int x)
        {
            N_Temp = x;
        }
        public void SetGen(string x)
        {
            Gen = x;
        }
        public void SetCre(string x)
        {
            Creador = x;
        }

        public string GetTit()
        {
            return Titulo;
        }
        public int GetNT()
        {
            return N_Temp;
        }
        public string GetGen()
        {
            return Gen;
        }
        public string GetCre()
        {
            return Creador;
        }
        public void entregar()
        {
            Entregado = true;
        }
        public void devolver()
        {
            Entregado = false;
        }
        public bool isEntregado()
        {
            return Entregado;
        }
        public Entregable CompareTo(Entregable xd)
        {
            if (xd is Serie)
            {
                Serie z = (Serie)xd;
                if (z.GetNT() > this.N_Temp)
                {
                    return z;
                }
                else
                {
                    return this;
                }
            }
            else if (xd is Videojuego)
            {
                Videojuego z = (Videojuego)xd;
                if (z.GetHE() > this.N_Temp)
                {
                    return z;
                }
                else
                {
                    return this;
                }
            }
            return null;
        }

    }
    class Videojuego : Entregable
    {
        //titulo, horas estimadas, entregado, genero y compañia.
        string Titulo = "";
        int Horas_estimadas = 10;
        bool Entregado = false;
        string Genero = "";
        string compañia = "";

        public Videojuego()
        {
        }
        public Videojuego(string T, int HE)
        {
            Titulo = T;
            Horas_estimadas = HE;
        }
        public Videojuego(string T, int HE, string G, string C)
        {
            Titulo = T;
            Horas_estimadas = HE;
            Genero = G;
            compañia = C;
        }
        public void SetTit(string x)
        {
            Titulo = x;
        }
        public void SetHE(int x)
        {
            Horas_estimadas = x;
        }
        public void SetGen(string x)
        {
            Genero = x;
        }
        public void SetComp(string x)
        {
            compañia = x;
        }

        public string GetTit()
        {
            return Titulo;
        }
        public int GetHE()
        {
            return Horas_estimadas;
        }
        public string GetGen()
        {
            return Genero;
        }
        public string GetComp()
        {
            return compañia;
        }

        public void entregar()
        {
            Entregado = true;
        }
        public void devolver()
        {
            Entregado = false;
        }
        public bool isEntregado()
        {
            return Entregado;
        }


        public Entregable CompareTo(Entregable xd)
        {
            if (xd is Serie)
            {
                Serie z = (Serie)xd;
                if (z.GetNT() > this.Horas_estimadas)
                {
                    return z;
                }
                else
                {
                    return this;
                }
            }
            else if (xd is Videojuego)
            {
                Videojuego z = (Videojuego)xd;
                if (z.GetHE() > this.Horas_estimadas)
                {
                    return z;
                }
                else
                {
                    return this;
                }
            }
            return null;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Serie[] series = new Serie[5];
            Videojuego[] videojuegos = new Videojuego[5];
            series[0] = new Serie("Serie", "Creador");
            series[1] = new Serie("a", 3, "b", "c");
            series[2] = new Serie("hola", "chau");
            series[3] = new Serie("dario", 6, "sanchez", "algo");
            series[4] = new Serie("si", 5, "no", "depende");
            videojuegos[0] = new Videojuego("Hell is other demons", 8);
            videojuegos[1] = new Videojuego("CSGO", 1500);
            videojuegos[2] = new Videojuego("Minecraft", 20);
            videojuegos[3] = new Videojuego("The Elder Scrolls V: Skyrim", 1000000000, "RPG", "Bethesda");
            videojuegos[4] = new Videojuego("LoL", 5464155);
            series[4].entregar();
            videojuegos[2].entregar();
            videojuegos[1].entregar();
            series[3].entregar();
            series[0].entregar();
            int juegos_entregados = 0, series_entregadas = 0;
            Serie mas_larga = series[0];
            Videojuego mas_largo = videojuegos[0];
            for (int i = 0; i < 5; i++)
            {
                if (series[i].isEntregado())
                {
                    series_entregadas++;
                    series[i].devolver();
                }
                if (videojuegos[i].isEntregado())
                {
                    juegos_entregados++;
                    videojuegos[i].devolver();
                }
                if (series[i].GetNT() > mas_larga.GetNT())
                {
                    mas_larga = series[i];
                }
                if (videojuegos[i].GetHE() > mas_largo.GetHE())
                {
                    mas_largo = videojuegos[i];
                }
            }
            Console.WriteLine(juegos_entregados + " videojuegos fueron entregados y " + series_entregadas + " series fueron entregadas.");
            Console.WriteLine("La serie con mas temporadas es: "+ mas_larga.GetTit());
            Console.WriteLine("El juego con mas horas estimadas es: " + mas_largo.GetTit());

        }
    }
}
