﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ej_3_8
{
    class Password
    {
        int Longitud = 8;
        string contraseña;

        public Password()
        {
           this.contraseña = GenerarContraseña();
            System.Threading.Thread.Sleep(1);
        }
        public Password(int L)
        {
            this.Longitud = L;
            this.contraseña = GenerarContraseña();
            System.Threading.Thread.Sleep(1);
        }

        private string GenerarContraseña()
        {
            
            var letras = "AGH5678NghijkOPQRSTZabcdeflmnopqBCDEFrstuvwxyz0123UVWXY49";
            var contraseñaArray = new char[this.Longitud];
            Random Pos = new Random(DateTime.UtcNow.Millisecond);
            for (int A = 0; A < this.Longitud; A++)
            {
                contraseñaArray[A] = letras[Pos.Next(letras.Length)];
            }

            string Nuevacontraseña = new string(contraseñaArray);
            return Nuevacontraseña;
        }

        private bool esMayus(char c)
        {
            string mayus = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            bool esMayus = false;
            foreach (char z in mayus)
            {
                if (c == z)
                {
                    esMayus = true;
                }
            }
            return esMayus;
        }
        private bool esNum(char c)
        {
            string mayus = "0123456789";
            bool esMayus = false;
            foreach (char z in mayus)
            {
                if (c == z)
                {
                    esMayus = true;
                }
            }
            return esMayus;
        }
        public bool Esfuerte()
        {
            int cont = 0;
            int contN = 0;
            int letras = this.contraseña.Length;

            foreach (char C in this.contraseña)
            {
                if (esMayus(C))
                    cont++;
            }
            foreach (char C in this.contraseña)
            {
                if (esNum(C))
                    contN++;
            }

            if (letras - cont - contN >= 1 && cont > 1 && contN > 4)
                return true;
            else
                return false;
        }
        public void SetContraseña(string C)
        {
            this.contraseña = C;
        }

        public string GetContraseña()
        {
            return this.contraseña;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ingrese la cantidad de contraseñas a generar");
            int tamaño = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese la longitud de las contraseñas a generar");
            int longitud = int.Parse(Console.ReadLine());
            Password[] contraseñas = new Password[tamaño];
            bool[] seguridad = new bool[tamaño];
            for(int i =0 ; i < tamaño; i++)
            {
                Password contraseña = new Password(longitud);
                contraseñas[i] = contraseña;

                seguridad[i] = contraseña.Esfuerte();
                Console.WriteLine("Contraseña | Es fuerte" + Environment.NewLine);

                Console.WriteLine(contraseñas[i].GetContraseña() + " " + contraseñas[i].Esfuerte());
            }
        }
    }
}
