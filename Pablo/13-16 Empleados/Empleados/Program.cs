﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empleados
{
    class Empleado
    {
        string Nombre;
        int Edad;
        int Salario;
        const int Plus = 300;

        protected Empleado(string nombre, int edad, int salario)
        {
            Nombre = nombre; Edad = edad; Salario = salario;
        }

        public int Get_Plus()
        {
            return Plus;
        }

        public int Get_Edad()
        {
            return Edad;
        }

        public string Get_Nombre()
        {
            return Nombre;
        }

        public int Get_Salario()
        {
            return Salario;
        }

        public void Set_Nombre(string x)
        {
            Nombre = x;
        }

        public void Set_Salario(int x)
        {
            Salario = x;
        }

        public void Set_Edad(int x)
        {
            Edad = x;
        }

    }




    class Repartidor : Empleado
    {
        string Zona;

        public Repartidor(string Nombre, int Edad, int Salario, string zona) : base(Nombre, Edad, Salario)
        {
            base.Set_Edad(Edad);
            base.Set_Nombre(Nombre);
            base.Set_Salario(Salario);
            Zona = zona;
        }

        public void Plus()
        {
            if (base.Get_Edad() < 25 && Zona == "3")
                base.Set_Salario(base.Get_Salario() + base.Get_Plus());
        }

        public void Set_Nom(string x)
        {
            Zona = x;
        }
        public string Get_Zona()
        {
            return Zona;

        }

    }


    class Comercial : Empleado
    {
        double Comision;

        public Comercial(string Nombre, int Edad, int Salario, double comision) : base(Nombre, Edad, Salario)
        {
            base.Set_Edad(Edad);
            base.Set_Nombre(Nombre);
            base.Set_Salario(Salario);
            Comision = comision;
        }

        public void Plus()
        {
            if (base.Get_Edad() > 30 && Comision > 200)
                base.Set_Salario(base.Get_Salario() + base.Get_Plus());
        }

        public void Set_Nom(double x)
        {
            Comision = x;
        }

        public double Get_Comision()
        {
            return Comision;

        }

    }

        class Program
        {
        static void Main(string[] args)
        {
            
            Comercial Objeto1 = new Comercial("juan", 31, 300, 220);
            Repartidor Objeto2 = new Repartidor("Juana", 24, 100, "3");
            while (true)
            {
                Console.WriteLine("Ingrese una opcion: " + Environment.NewLine + " 1 para mostrar los datos, 2 para aplicar un plus y 3 para salir");


                string decision = Console.ReadLine();

                if (decision == "1")
                {
                    Console.Clear();
                    Console.WriteLine("nombre: " + Objeto1.Get_Nombre() + " Edad: " + Objeto1.Get_Edad() + " Salario: " + Objeto1.Get_Salario() + " Comision: " + Objeto1.Get_Comision());
                    Console.WriteLine("nombre: " + Objeto2.Get_Nombre() + " Edad: " + Objeto2.Get_Edad() + " Salario: " + Objeto2.Get_Salario() + " Zona: " + Objeto2.Get_Zona());
                }


                if (decision == "2")
                {
                    Console.Clear();
                    Objeto1.Plus();
                    Objeto2.Plus();
                }

                if (decision == "3")
                    Environment.Exit(1);
            }
        }
    }
}
