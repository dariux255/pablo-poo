﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ej_15
{
    class Bebidas
    {
        int ID;
        int Litros;
        int Precio;
        string Marca;

        protected Bebidas(int id, int litros, int precio, string marca)
        {
            ID = id;
            Litros = litros;
            Precio = precio;
            Marca = marca;
        }
        public int getPrecio()
        {
            return Precio;
        }
        public string getMarca()
        {
            return Marca;
        }
        public int getID()
        {
            return ID;
        }
    }

    class Mineral : Bebidas 
    {
        string Origen;
        public Mineral(string origen, int id, int litros, int precio, string marca) : base(id, litros, precio, marca)
        {
            Origen = origen;
        }
    }

    class Azucaradas : Bebidas
    {
        int P_Azucar;
        bool Oferta;
        public Azucaradas(int azucar, bool oferta, int id, int litros, int precio, string marca) : base(id, litros, precio, marca)
        {
            P_Azucar = azucar;
            Oferta = oferta;
        }
    }


    class Estanteria
    {
        List<List<Bebidas>> Bebidas = new List<List<Bebidas>>();
        public Estanteria(List<List<Bebidas>> bebidas)
        {
            Bebidas = bebidas;
        }

        public int CalcularPrecio()
        {
            return 5;
        }

        public int CalcularPrecioMarca()
        {
            Console.WriteLine("ingrese la marca a calcular");
            int Precio = 0;
            string busqueda = Console.ReadLine();
            foreach (var i in Bebidas)
                foreach (var j in i)
                    if (j.getMarca() == busqueda)
                        Precio += j.getPrecio();

            return Precio;
        }

        public int CalcularPrecioEstanteria()
        {
            int Precio = 0;
            foreach (var i in Bebidas)
                foreach (var j in i)
                    Precio += j.getPrecio();
            return Precio;
        }

        public bool EstanteLleno()
        {
            if (Bebidas[Bebidas.Count()-1][4] is null)
                return false;
            else return true;
        }

        public void AgregarProducto(Bebidas bebida)
        {
            if (EstanteLleno() == false)
                Bebidas[Bebidas.Count()].Add(bebida);
            else Console.WriteLine("las estanterias estan llenas");

        }

        public void EliminarProducto()
        {
            Console.WriteLine("ingrese id a eliminar");
            foreach (var i in Bebidas)
                for (int j = i.Count(); j>=0; j--)
                    if (i[j].getID() == int.Parse(Console.ReadLine()))
                        i.RemoveAt(j);
        }




    }
    class Program
    {
        static void Main(string[] args)
        {
            List<List<Bebidas>> Bebidas = new List<List<Bebidas>>();
            List<Bebidas> Estante1 = new List<Bebidas>();
            List<Bebidas> Estante2 = new List<Bebidas>();

            for (int i = 0; i < 5; i++)
            {
                Azucaradas gaseosa = new Azucaradas(100, false, 1, 12, 1000+i, "tu mama");
                Estante1.Add(gaseosa);
            }
            for (int i = 0; i < 5; i++)
            {
                Mineral mineral = new Mineral("los andes", 5, 3, 1999+i, "tu jermu");
                Estante2.Add(mineral);
            }

            Bebidas.Add(Estante1);
            Bebidas.Add(Estante2);

            Estanteria Estanteria = new Estanteria(Bebidas);

            Azucaradas agregar = new Azucaradas(100, false, 1, 12, 69420, "prueba");

          //  Estanteria.AgregarProducto(agregar);

        }
    }
}
